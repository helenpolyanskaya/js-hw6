// 1) Екранування це коли деякі сімволи не считиваються, тому потрібен символ який екранірує. Наприклад лапки: '
// ' I\'m Elena '

// 2) Function declaration , Named Function expression , Function expression;

// 3) Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду;


function createNewUser( ) {
    let name= prompt ("What is your name?");
    let surname = prompt ("What is your surname?");
    let dob =  prompt("What is the date of your Birthday?" , "dd.mm.yyyy");
    

    return  {
        _firstName: name,
        _lastName: surname,
        birthday: dob,

        getLogin: function() {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },

        get firstName() {
            return this._firstName;
        },

        setFirstName(name) {
             this._firstName = name;
        },

        get lastName() {
            return this._lastName;
        },

        setLastName(name) {
             this._lastName = name;
        },  

        getAge: function () {
            let birthdayDay = this.birthday.slice(0,2);
            let birthdayMonth = this.birthday.slice(3,5);
            let birthdayYear = this.birthday.slice(6);
            let birthdayNew = birthdayMonth+"."+birthdayDay+"."+birthdayYear;
            const was = new Date (birthdayNew);
            let now = new Date();
            now.setDate(now.getDate() - was.getDate());
            now.setFullYear(now.getFullYear()-was.getFullYear());
            now.setMonth(now.getMonth()-was.getMonth());
            let ageUser = now.getFullYear();
            return ageUser;
        },

        getPassword: function() {
            return this._firstName.charAt(0).toUpperCase() + this._lastName.toLowerCase() + this.birthday.slice(6);
        },    
    };   
}             

let newUser = createNewUser();


console.log(newUser);
console.log(newUser.getLogin()); 
console.log("---------------------------");
console.log("----Without functions:-----")
newUser.firstName = prompt ("Change your name...")
newUser.lastName = prompt ("Change your surname...")
console.log(newUser);
console.log("------------------------");
console.log("----Whith functions:----")
newUser.setFirstName ( prompt ("Change your name..."));
newUser.setLastName ( prompt ("Change your surname..."));
console.log(newUser);
console.log("---------------------------");
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());